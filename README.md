## GoMaterialPalette

Material Palette app using Go and Qt UI. 

This project is using https://github.com/therecipe/qt library.


### How To Build

- Install the thericipe/qt library (See https://github.com/therecipe/qt#installation)
- Run the following command in console:
    `qtdeploy test desktop`


License under GPLV3

